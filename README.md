# new-gml-site

New GML site

[Адрес сайта https://faizov_boris.gitlab.io/new-gml-site/](https://faizov_boris.gitlab.io/new-gml-site/)

Запуск сайта локально:
```
pelican content -s pelicanconf.py -t ./themes/simple --debug && cp output/home.html output/index.html && pelican --listen
```


## Id и имена людей (для быстрого редактирования связей):

| Id | Имя человека |
|---|---|
| anton_konushin | Антон Конушин |
| dmitriy_vatolin | Дмитрий Ватолин |
| vladimir_frolov | Владимир Фролов |
| vlad_shakhuro | Влад Шахуро |
| olga_senyukova | Ольга Сенюкова |
| mikhail_erofeev | Михаил Ерофеев |
| anna_sokolova | Анна Соколова |
| olga_barinova | Ольга Баринова |
| denis_zobnin | Денис Зобнин |


## Id и названия проектов (для быстрого редактирования связей):

| Id | Project Name |
|---|---|
| traffic_sign_recognition | Traffic sign recognition |
| video_matting_benchmark | Video Matting Benchmark |
| human_gait_recognition_in_video | Human gait recognition in video |
| medical_image_analysis | Medical image analysis |
| traffic_sign_recognition_with_synthetic_datasets | Traffic sign recognition with synthetic datasets |
| text_detection_and_recognition_in_natural_images | Text detection and recognition in natural images |
| geometric_image_parsing_in_man_made_environments | Geometric image parsing in man-made environments |
