# -*- coding: utf-8 -*-

import logging
import os
import os.path
import copy
import csv
import bibtexparser
from collections import defaultdict
import functools
from operator import attrgetter

from pelican import signals
from pelican.contents import Content
from pelican.generators import ArticlesGenerator


def FindTranslation(obj, lang):
    for lang_obj in [obj] + obj.translations:
        if lang_obj.lang == lang:
            return lang_obj
    return None

def GetLangPageUrl(settings, slug, lang=None):
    if lang == settings['DEFAULT_LANG'] or lang is None:
        url = '{slug}.html'.format(slug=slug)
    else:
        url = '{slug}-{lang}.html'.format(slug=slug, lang=lang)
    return url

def GetAllTranslationsUrls(settings, slug, cur_lang=None):
    translations = []
    for tr_lang in settings['POSSIBLE_LANGS']:
        if tr_lang == cur_lang:
            continue
        translations.append({
            'lang': tr_lang,
            'url': GetLangPageUrl(settings, slug, tr_lang)
        })
    return translations

def GetExistingTranslationsUrls(obj, url_field, cur_lang=None):
    translations = []
    for translation in obj.translations:
        translations.append({
            'lang': translation.lang,
            'url': getattr(translation, url_field)
        })
    return translations

def GetListOfAllTranslations(objs):
    return sum([obj.translations for obj in objs], objs)


class Holder(object):
    # First element always should be 'id'
    SPECIFIC_LANGUAGE_FIELDS = []
    COMMON_LANGUAGE_FIELDS = ['id']
    category_name = ''

    def __init__(self, categories, settings):
        self.settings = settings
        articles = sum([v for k, v in categories if k == self.category_name], [])
        self.objects = []
        self.id_to_objects_pos = {}
        for article in articles:
            if hasattr(article, 'active') and article.active != 'yes':
                continue
            self.id_to_objects_pos[article.id] = len(self.objects)
            self.objects.append(article)
        self.missing_ids_info = {}
    
    def get_info_by_ids(self, need_ids, need_lang=None):
        try_suffixes = ['']
        if need_lang:
            try_suffixes.append('_' + need_lang)
        else:
            try_suffixes.append('_en')

        return_infos = []
        for cur_id in need_ids:
            cur_object = None
            any_lang_object = None
            if cur_id in self.id_to_objects_pos.keys():
                any_lang_object = self.objects[self.id_to_objects_pos[cur_id]]
                if need_lang is not None:
                    cur_object = FindTranslation(any_lang_object, need_lang)
                else:
                    cur_object = any_lang_object
            if cur_object is None:
                cur_return_info = {
                    field_name: 'None' for field_name in self.COMMON_LANGUAGE_FIELDS + self.SPECIFIC_LANGUAGE_FIELDS
                }
                if any_lang_object is not None:
                    cur_return_info.update({
                        field_name: getattr(any_lang_object, field_name) for field_name in self.COMMON_LANGUAGE_FIELDS
                    })
                if cur_id in self.missing_ids_info:
                    for field_name in self.COMMON_LANGUAGE_FIELDS + self.SPECIFIC_LANGUAGE_FIELDS:
                        for suffix in try_suffixes:
                            if field_name + suffix in self.missing_ids_info[cur_id]:
                                cur_return_info[field_name] = self.missing_ids_info[cur_id][field_name + suffix]
                                break

                cur_return_info['id'] = cur_id
                return_infos.append(cur_return_info)
            else:
                return_infos.append({
                    field_name: getattr(cur_object, field_name) for field_name in self.COMMON_LANGUAGE_FIELDS + self.SPECIFIC_LANGUAGE_FIELDS
                })
        return return_infos

    '''
    def get_objects_by_ids(self, need_ids, need_lang=None):
        return_objects = []
        for cur_id in need_ids:
            cur_object = None
            if cur_id in self.id_to_objects_pos.keys():
                cur_object = self.objects[self.id_to_objects_pos[cur_id]]
                if need_lang is not None:
                    cur_object = FindTranslation(cur_object, need_lang)
            if cur_object is None:
                return_objects.append([cur_id, 'None'])
            else:
                return_objects.append([cur_id, cur_object])
        return return_objects
    '''


class PeopleHolder(Holder):
    SPECIFIC_LANGUAGE_FIELDS = ['name', 'person_url']
    COMMON_LANGUAGE_FIELDS = ['id']
    category_name = 'people'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [persontype, id, title, name, email, photo, position, researchinterests,
         projects, selectedpublications, about, url]
        '''
        super().__init__(categories, settings)
        for person in GetListOfAllTranslations(self.objects):
            person.about = person.content
            person.name = person.title
            person.researchinterests = list(person.researchinterests.strip().split(', '))
            person.projects = list(person.projects.strip().split(', '))
            person.selectedpublications = list(person.selectedpublications.strip().split(', '))
            person.person_url = 'people/' + GetLangPageUrl(self.settings, person.id, person.lang)
        
        with open(settings['PATH_TO_MISSING_ID_TO_PEOPLE_INFO'], "r") as fp:
            reader = csv.DictReader(fp)
            for row in reader:
                self.missing_ids_info[row['id']] = row


class ProjectsHolder(Holder):
    SPECIFIC_LANGUAGE_FIELDS = ['title', 'project_url']
    COMMON_LANGUAGE_FIELDS = ['id', 'smallphoto']
    category_name = 'projects'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [projecttype, id, smallphoto, photo,
         title, contactperson, team, selectedpublications, content]
        '''
        super().__init__(categories, settings)
        for project in GetListOfAllTranslations(self.objects):
            project.selectedpublications = list(project.selectedpublications.strip().split(', '))
            project.team = list(project.team.strip().split(', '))
            project.project_url = 'projects/' + GetLangPageUrl(self.settings, project.id, project.lang)
        
        with open(settings['PATH_TO_MISSING_ID_TO_PROJECTS_INFO'], "r") as fp:
            reader = csv.DictReader(fp)
            for row in reader:
                self.missing_ids_info[row['id']] = row


class MyPublicationEntry:
    def __init__(self, bib_entry, lab_group_id):
        db = bibtexparser.bibdatabase.BibDatabase()
        db.entries = [copy.copy(bib_entry)]

        self.id = bib_entry['ID']
        self.title = bib_entry['title']
        self.year = bib_entry['year']
        self.paperauthors = [bib_entry['author']]
        self.lang = bib_entry['language'][:2]
        self.labgroupids = [lab_group_id]
        self.translations = []
        self.downloadlink = 'None'
        #self.image = 'images/traffic_sign_recognition_small.jpg'
        self.image = 'None'

        if 'booktitle' in bib_entry:
            self.journal = bib_entry['booktitle']
        elif 'journal' in bib_entry:
            self.journal = bib_entry['journal']

        if 'annote' in bib_entry:
            self.content = bib_entry['annote']
            del db.entries[0]['annote']
        self.bibtex = bibtexparser.dumps(db)


class PublicationsHolder(Holder):
    SPECIFIC_LANGUAGE_FIELDS = ['paperauthors']
    COMMON_LANGUAGE_FIELDS = ['id', 'title', 'journal', 'downloadlink']
    category_name = 'publications'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [id, photo, title, paperauthors, journal, downloadlink, content]
        '''
        super().__init__(categories, settings)
        for publication in GetListOfAllTranslations(self.objects):
            #for i, line in enumerate(publication.bibtex[:-1]):
            #    publication.bibtex[i] = line + "<br>"
            #publication.bibtex = ''.join(publication.bibtex)
            publication.paperauthors = list(publication.paperauthors.strip().split(', '))
            publication.labgroupids = list(publication.labgroupids.strip().split(', '))

        bib_publication_entries = []
        bib_publication_entries_ids = {}

        for lab_group_info in settings['LAB_GROUPS_META_INFO']:
            with open(lab_group_info['bib_path'], 'r') as fp:
                bib_database = bibtexparser.bparser.BibTexParser().parse_file(fp)
                for bib_entry in bib_database.entries:
                    cur_publication = MyPublicationEntry(bib_entry, lab_group_info['group_id'])
                    if cur_publication.id in bib_publication_entries_ids:
                        bib_publication_entries[bib_publication_entries_ids[cur_publication.id]].labgroupids.append(lab_group_info)
                    else:
                        bib_publication_entries.append(cur_publication)
                        bib_publication_entries_ids[cur_publication.id] = len(bib_publication_entries) - 1

        for publication in GetListOfAllTranslations(self.objects):
            if publication.bibtexid in bib_publication_entries_ids:
                publication.bibtex = bib_publication_entries[bib_publication_entries_ids[publication.bibtexid]].bibtex
                bib_publication_entries[bib_publication_entries_ids[publication.bibtexid]] = None
                self.id_to_objects_pos[publication.bibtexid] = self.id_to_objects_pos[publication.id]
            else:
                publication.bibtex = publication.bibtexid

        for cur_publication in bib_publication_entries:
            if cur_publication is None:
                continue
            self.objects.append(cur_publication)
            self.id_to_objects_pos[cur_publication.id] = len(self.objects) - 1

        for publication in GetListOfAllTranslations(self.objects):
            publication.translations = []
            for lang in self.settings['POSSIBLE_LANGS']:
                if lang == self.settings['DEFAULT_LANG']:
                    continue
                lang_publication = copy.copy(publication)
                lang_publication.lang = lang
                publication.translations.append(lang_publication)
            


class NewsHolder(Holder):
    SPECIFIC_LANGUAGE_FIELDS = []
    COMMON_LANGUAGE_FIELDS = ['id']
    category_name = 'news'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [title, id, people_names, date,
         summary, summaryImage, content]
        '''
        super().__init__(categories, settings)
        for cur_news in GetListOfAllTranslations(self.objects):
            cur_news.people_names = [author.slug for author in cur_news.authors]


class CoursesHolder(Holder):
    SPECIFIC_LANGUAGE_FIELDS = ['title', 'teachers']
    COMMON_LANGUAGE_FIELDS = ['id', 'image']
    category_name = 'courses'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [title, id, teachers, image, content]
        '''
        super().__init__(categories, settings)
        for cur_course in GetListOfAllTranslations(self.objects):
            cur_course.teachers = list(cur_course.teachers.strip().split(', '))


class DataHolder(object):
    def __init__(self, categories, settings):
        self.projects_holder = ProjectsHolder(categories, settings)
        self.people_holder = PeopleHolder(categories, settings)
        self.publications_holder = PublicationsHolder(categories, settings)
        self.news_holder = NewsHolder(categories, settings)
        self.courses_holder = CoursesHolder(categories, settings)

        for publication_article in GetListOfAllTranslations(self.publications_holder.objects):
            publication_article.paperauthors = self.people_holder.get_info_by_ids(publication_article.paperauthors, need_lang=publication_article.lang)

        for news_article in GetListOfAllTranslations(self.news_holder.objects):
            news_article.people_names = self.people_holder.get_info_by_ids(news_article.people_names, need_lang=news_article.lang)

        for course_article in GetListOfAllTranslations(self.courses_holder.objects):
            course_article.teachers = self.people_holder.get_info_by_ids(course_article.teachers, need_lang=course_article.lang)

        for person_article in GetListOfAllTranslations(self.people_holder.objects):
            person_article.projects = self.projects_holder.get_info_by_ids(person_article.projects, need_lang=person_article.lang)
            person_article.selectedpublications = self.publications_holder.get_info_by_ids(person_article.selectedpublications, need_lang=person_article.lang)

        for project_article in GetListOfAllTranslations(self.projects_holder.objects):
            project_article.contactperson = self.people_holder.get_info_by_ids([project_article.contactperson], need_lang=project_article.lang)
            if len(project_article.contactperson) > 0:
                project_article.contactperson = project_article.contactperson[0]
            project_article.team = self.people_holder.get_info_by_ids(project_article.team, need_lang=project_article.lang)
            project_article.selectedpublications = self.publications_holder.get_info_by_ids(project_article.selectedpublications, need_lang=project_article.lang)

class MyGenerator(ArticlesGenerator):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate_output(self, writer):
        # print([(elem.url, [tr.url for tr in elem.translations]) for elem in self.articles])
        data = DataHolder(self.categories, self.settings)
        write = functools.partial(writer.write_file, relative_urls=self.settings['RELATIVE_URLS'], context=self.context)

        self.generate_homepage(write, data)
        self.generate_news(write, data)
        self.generate_people_page(write, data)
        self.generate_persons(write, data)
        self.generate_projects_page(write, data)
        self.generate_projects(write, data)
        self.generate_publications_page(write, data)
        self.generate_teaching_page(write, data)
        signals.article_writer_finalized.send(self, writer=writer)


    def generate_homepage(self, write, data):
        news_articles = sum([v for k, v in self.categories if k == 'news'], [])

        for lang in self.settings['POSSIBLE_LANGS']:
            url = save_as = GetLangPageUrl(self.settings, 'home', lang)
            translations = GetAllTranslationsUrls(self.settings, 'home', lang)
            all_news_link=GetLangPageUrl(self.settings, 'newslist', lang)
            
            current_news = [FindTranslation(news, lang) for news in data.news_holder.objects if FindTranslation(news, lang) is not None]
            current_news.sort(key=attrgetter('date'), reverse=True)
            current_news = current_news[:3]

            write(save_as, self.get_template('home'), template_name='home',
                articles=current_news, lang=lang, translations=translations, all_news_link=all_news_link,
                page_name=os.path.splitext(save_as)[0], url=url)


    def generate_news(self, write, data):
        news_articles = sum([v for k, v in self.categories if k == 'news'], [])

        for lang in self.settings['POSSIBLE_LANGS']:
            url = save_as = GetLangPageUrl(self.settings, 'newslist', lang)
            translations = GetAllTranslationsUrls(self.settings, 'newslist', lang)
            
            current_news = [FindTranslation(news, lang) for news in data.news_holder.objects if FindTranslation(news, lang) is not None]
            write(save_as, self.get_template('news'), template_name='news',
                articles=current_news, lang=lang, translations=translations,
                page_name=os.path.splitext(save_as)[0], url=url)


    def generate_people_page(self, write, data):
        PERSON_PAGES_NAMES = ['researchers', 'students']
        PERSON_TYPES = ['researcher', 'student']
        
        for page_name, person_type in zip(PERSON_PAGES_NAMES, PERSON_TYPES):
            for lang in self.settings['POSSIBLE_LANGS']:
                url = save_as = GetLangPageUrl(self.settings, page_name, lang)
                translations = GetAllTranslationsUrls(self.settings, page_name, lang)

                current_people = [person for person in data.people_holder.objects if person.persontype == person_type]
                current_people = [FindTranslation(person, lang) for person in current_people if FindTranslation(person, lang) is not None]
                current_people.sort(key = lambda x: float(x.personorder))

                write(save_as, self.get_template('people_page'), template_name='people_page',
                    people=current_people, lang=lang, translations=translations,
                    page_name=os.path.splitext(save_as)[0], url=url)


    def generate_persons(self, write, data):
        for person_article in GetListOfAllTranslations(data.people_holder.objects):
            save_as = person_article.person_url
            url = person_article.person_url
            write(save_as, self.get_template('person_page'), template_name='person_page',
                  person_info=person_article, lang=person_article.lang, translations=GetExistingTranslationsUrls(person_article, 'person_url'),
                  page_name=os.path.splitext(save_as)[0], url=url)


    def generate_projects_page(self, write, data):
        PROJECT_TYPES = ['ongoing', 'completed']
        PROJECT_TYPES_NAMES = ['projects_ongoing', 'projects_completed']

        for lang in self.settings['POSSIBLE_LANGS']:
            projects_output = {}
            for project_type, project_type_name in zip(PROJECT_TYPES, PROJECT_TYPES_NAMES):
                url = save_as = GetLangPageUrl(self.settings, 'projects', lang)
                translations = GetAllTranslationsUrls(self.settings, 'projects', lang)
                
                current_projects = [project for project in data.projects_holder.objects if project.projecttype == project_type]
                current_projects = [FindTranslation(project, lang) for project in current_projects if FindTranslation(project, lang) is not None]
                current_projects.sort(key = lambda x: float(x.projectorder), reverse=True)
                projects_output[project_type_name] = current_projects

            write(save_as, self.get_template('projects_page'), template_name='projects_page',
                lang=lang, translations=translations, **projects_output,
                page_name=os.path.splitext(save_as)[0], url=url)


    def generate_projects(self, write, data):
        for project_article in GetListOfAllTranslations(data.projects_holder.objects):
            save_as = project_article.project_url
            url = project_article.project_url
            write(save_as, self.get_template('project_page'), template_name='project_page',
                  project_info=project_article, lang=project_article.lang, translations=GetExistingTranslationsUrls(project_article, 'project_url'),
                  page_name=os.path.splitext(save_as)[0], url=url)


    def generate_publications_page(self, write, data):
        for lang in self.settings['POSSIBLE_LANGS']:
            url = save_as = GetLangPageUrl(self.settings, 'publications', lang)
            translations = GetAllTranslationsUrls(self.settings, 'publications', lang)

            all_publications_list = [FindTranslation(publication, lang) for publication in data.publications_holder.objects if FindTranslation(publication, lang) is not None]
            publications_by_lab_groups = []
            for lab_group_info in self.settings['LAB_GROUPS_META_INFO']:
                cur_publications_list = [publication for publication in all_publications_list if lab_group_info['group_id'] in publication.labgroupids]
                # list.sort must be stable!
                def papers_comparator(x, y):
                    if hasattr(x, 'date') and hasattr(y, 'date'):
                        return x.date > y.date
                    else:
                        return x.year > y.year
                cur_publications_list.sort(key=functools.cmp_to_key(papers_comparator))
                cur_publications_list = [[publication.id, publication] for publication in cur_publications_list]
                publications_by_lab_groups.append((lab_group_info['group_title'][lang], cur_publications_list))

            write(save_as, self.get_template('publications_page'), template_name='publications_page',
                publications_by_lab_groups=publications_by_lab_groups, lang=lang, translations=translations,
                page_name=os.path.splitext(save_as)[0], url=url)


    def generate_teaching_page(self, write, data):
        for lang in self.settings['POSSIBLE_LANGS']:
            url = save_as = GetLangPageUrl(self.settings, 'teaching', lang)
            translations = GetAllTranslationsUrls(self.settings, 'teaching', lang)
            
            current_courses = [FindTranslation(course, lang) for course in data.courses_holder.objects if FindTranslation(course, lang) is not None]
            current_courses.sort(key = lambda x: float(x.courseorder), reverse=False)

            write(save_as, self.get_template('teaching_page'), template_name='teaching_page',
                lang=lang, translations=translations, current_courses=current_courses,
                page_name=os.path.splitext(save_as)[0], url=url)


def create_generator(pelican_object):
    return MyGenerator


def fix_default_articles(article_generator):
    if isinstance(article_generator, MyGenerator):
        return
    # Чтобы добавить новые поля (и изменить существующие) к новостям нужно сконструировать DataHolder
    DataHolder(article_generator.categories, article_generator.settings)
    # Чтобы оставить только новости (так как дефолтным генератором делаются только они)
    article_generator.articles = [article for article in article_generator.articles if article.category=='news']
    article_generator.translations = [article for article in article_generator.translations if article.category=='news']

def register():
    signals.get_generators.connect(create_generator)
    signals.article_generator_finalized.connect(fix_default_articles)
