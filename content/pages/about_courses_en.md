Active: yes
Title: General information
Category: about_courses
Id: about_courses_en
Lang: en

Since 1994 "Computer graphics" is a mandatory course for all students in our department. Since 2005 members of our lab teach elective courses on main research directions, including computer vision, video processing and photorealistic computer graphics. In 2007 we launch our own web system "http://courses.graphicon.ru" that provide web support for our courses. Members of our lab also teach courses at Yandex School for Data Analysis.
