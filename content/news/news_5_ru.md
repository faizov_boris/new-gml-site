Active: yes
Title: Новые студенты 2020
Category: news
Authors: anton_konushin, personX
Date: 2020-04-16 10:00
Id: news_number_5
Lang: ru
Summary: Новые студенты 2020
SummaryImage: images/lab_logo.jpg

По результатам собеседования 16.04.2020 для распределения на кафедру ИИТ в лабораторию КГиМ рекомендуются следующие студенты:

1) Бадмаев Тингир Мингиянович 206
2) Сидоров Евгений Максимович 202
3) Офицеров Владислав Алексеевич 212
4) Эмиров Артур Станиславович 204
5) Мещанинов Вячеслав Павлович 203
6) Сафонов Николай Ильич 202
7) Молотилов Никита Николаевич 210
8) Дремин Михаил Витальевич 201
9) Кириллова Анастасия Павловна 202
10) Гарифуллин Альберт Рустемович 209
11) Межов Ян Максимович 206
12) Галушко Павел Дмитриевич 209
13) Пятковский Сергей Александрович 202
14) Гущин Александр Евгеньевич 202
15) Вартанов Дмитрий Александрович 203
16) Зеленцов Алексей Викторович 201

