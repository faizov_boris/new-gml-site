Active: yes
Title: New students 2020
Category: news
Authors: anton_konushin, personX
Date: 2020-04-16 10:00
Id: news_number_5
Lang: en
Summary: New students 2020
SummaryImage: images/lab_logo.jpg

Based on the results of the interview on 04/16/2020, the following students are recommended for distribution to the IIT department in the GML laboratory:

1) Badmaev Tingir Mingiyanovich 206
2) Sidorov Evgeny Maksimovich 202
3) Officers Vladislav Alekseevich 212
4) Emirov Artur Stanislavovich 204
5) Meshchaninov Vyacheslav Pavlovich 203
6) Safonov Nikolay Ilyich 202
7) Molotilov Nikita Nikolaevich 210
8) Dremin Mikhail Vitalievich 201
9) Kirillova Anastasia Pavlovna 202
10) Garifullin Albert Rustemovich 209
11) Mezhov Yan Maksimovich 206
12) Galushko Pavel Dmitrievich 209
13) Pyatkovsky Sergey Alexandrovich 202
14) Gushchin Alexander Evgenievich 202
15) Vartanov Dmitry Alexandrovich 203
16) Zelentsov Alexey Viktorovich 201

