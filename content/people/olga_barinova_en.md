Active: yes
Category: people
PersonType: alumni_researcher
Id: olga_barinova
Lang: en
Title: Olga Barinova
PersonOrder: 40
Position: Researcher
Email: obarinova@graphics.cs.msu.ru
Photo: images/people_images/olga_barinova.jpg
ResearchInterests: Computer vision, Machine Learning
Projects: geometric_image_parsing_in_man_made_environments
SelectedPublications: 

I got my PhD from Moscow State University in 2010 and now work in Graphics&Media Lab. My research interests lie in computer vision and machine learning. I have participated in a number of joint research projects with different companies, including Samsung Advanced Institute of Technology and Microsoft Research.
