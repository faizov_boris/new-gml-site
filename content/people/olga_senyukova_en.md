Active: yes
Category: people
PersonType: researcher
Id: olga_senyukova
Lang: en
Title: Olga Senyukova
PersonOrder: 20
Position: Teaching assistant
Email: osenyukova@graphics.cs.msu.ru
Photo: images/people_images/olga_senyukova.jpg
ResearchInterests: computer vision, machine learning, biomedical image analysis
Projects: medical_image_analysis
SelectedPublications: gavrishchaka2015multi-complexity6759819, senyukova2014segmentation6529757, gavrishchaka2013robust3400655, gavrishchaka2011ensemble557171

I am an assistant professor of the department of Computing Systems and Automation, Faculty of Computational Mathematics and Cybernetics, Lomonosov Moscow State University. I received my Ph.D. from CMC MSU in 2012.

I am the author of 28 research papers, laureate of the faculty scholarship (2013, 2014, 2016) and MSU scholarship (2015) for young scientists and teachers that achieved significant success in teaching and research activities. In 2015 I won the contest on receiving the Russian President scholarship for young scientists and PhD students. I participate in research projects of Graphics and Media Lab. I am the member of organizing and program committees of international summer schools.

PhD Thesis: Algorithms for semantic segmentation and classification of low-dimensional biomedical signals based on machine learning
