Active: yes
Category: people
PersonType: researcher
Id: anton_konushin
Lang: en
Title: Anton S. Konushin
PersonOrder: 0
Position: Associate professor
Email: ktosh@graphics.cs.msu.ru
Photo: images/people_images/anton_konushin.jpg
ResearchInterests: computer vision, video surveillance, image-based 3D modeling, semantic analysis of images and 3D point clouds
Projects: traffic_sign_recognition, human_gait_recognition_in_video, traffic_sign_recognition_with_synthetic_datasets
SelectedPublications: eval_of_traffic_sign_rec, konushin2013a-system5112364, rother2013alpha-flow3716677, konushin2011people3716807

I am an associate professor and head of Graphics & Media Lab, Faculty of Computational Mathematics and Cybernetics , Lomonosov Moscow State University.

I’am also an associate professor in National Research University Higher School of Economics.

I recieved my Ph.D. from Keldysh Institute for Applied Mathematics Russian Academy of Science in 2005. I joined Lomonosov Moscow State University in 2005. Since 2010 I’m also a lecturer in Yandex school for data analysis. In 2014 I joined National Research University Higher School of Economics.

I’m also a scientific consultant in “Video Analysis Technologies” LLC , which is a resident of Skolkovo IT cluster.
