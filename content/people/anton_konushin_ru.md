Active: yes
Category: people
PersonType: researcher
Id: anton_konushin
Lang: ru
Title: Антон Конушин
PersonOrder: 0
Position: Associate professor
Email: ktosh@graphics.cs.msu.ru
Photo: images/people_images/anton_konushin.jpg
ResearchInterests: компьютерное зрение, видеонаблюдение, 3D-моделирование на основе изображений, семантический анализ изображений и 3D-облака точек
Projects: traffic_sign_recognition, human_gait_recognition_in_video, traffic_sign_recognition_with_synthetic_datasets
SelectedPublications: eval_of_traffic_sign_rec, konushin2013a-system5112364, rother2013alpha-flow3716677, konushin2011people3716807


Я доцент и руководитель лаборатории Компьютерной Графики и Мультимедиа факультета Вычислительной Математики и Кибернетики МГУ им. М.В. Ломоносова.

Я также доцент Национального Исследовательского Университета «Высшая школа экономики».

Я получил степень кандидата наук в Институте прикладной математики им. М.В. Келдыша Российской академии наук в 2005 году. Я поступил в МГУ им. М.В. Ломоносова в 2005 году. С 2010 года я также преподаю в Школе Анализа Данных Яндекса. В 2014 году я начал работать в Национальном исследовательском университете «Высшая школа экономики».

Я также являюсь научным консультантом в ООО «Технологии видеоанализа», которое является резидентом ИТ-кластера Сколково.
