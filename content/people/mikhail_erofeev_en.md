Active: yes
Category: people
PersonType: researcher
Id: mikhail_erofeev
Lang: en
Title: Mikhail Erofeev
PersonOrder: 40
Position: Junior researcher
Email: merofeev@graphics.cs.msu.ru
Photo: images/people_images/mikhail_erofeev.jpg
ResearchInterests: Video and Image Processing, Machine Learning
Projects: 
SelectedPublications: gitman2015perceptually10724982, alexander2014automatic6319795, gitman2014semiautomatic6876185

About Mikhail Erofeev

PhD Thesis: Development of methods of high-quality stereo generation for scences with semitransparent edges

## Scholarships and grants

- The Russian President scholarship for young scientists and PhD students. (2015-2017)
- MSU scholarship for young teachers and researchers (2015)
- Grant U.M.N.I.K, "Stereo image processing for 3D enabled cell phones" (2014-2015)