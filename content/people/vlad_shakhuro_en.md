Active: yes
Category: people
PersonType: researcher
Id: vlad_shakhuro
Lang: en
Title: Vladislav Shakhuro
PersonOrder: 15
Position: Junior researcher
Email: vlad.shakhuro@graphics.cs.msu.ru
Photo: images/people_images/vlad_shakhuro.jpg
ResearchInterests: Computer vision, Machine Learning
Projects: traffic_sign_recognition, traffic_sign_recognition_with_synthetic_datasets
SelectedPublications: konushin2020klassifikatsiya296617493, konushin2018sintez223274406, konushin2018sintez107538848, konushin2016rossiiskaya22427117

About Vlad Shakhuro
