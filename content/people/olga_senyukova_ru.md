Active: yes
Category: people
PersonType: researcher
Id: olga_senyukova
Lang: ru
Title: Ольга Сенюкова
PersonOrder: 20
Position: Ассистент
Email: osenyukova@graphics.cs.msu.ru
Photo: images/people_images/olga_senyukova.jpg
ResearchInterests: компьютерное зрение, машинное обучение, анализ биомедицинских изображений
Projects: medical_image_analysis
SelectedPublications: gavrishchaka2015multi-complexity6759819, senyukova2014segmentation6529757, gavrishchaka2013robust3400655, gavrishchaka2011ensemble557171

Кандидат физико-математических наук. Ассистент кафедры АСВК. Автор 28 научных публикаций. Лауреат стипендии стипендии ВМК (2013, 2014, 2016) и МГУ (2015) для молодых ученых и преподавателей, добившихся значительных успехов в педагогической и научной деятельности. Победитель конкурса 2015-2017 на получение стипендии Президента РФ молодым ученым и аспирантам. Принимает участие в проектах лаборатории компьютерной графики и мультимедиа. Член организационного и программного комитетов международных летних школ.

Диссертация кандидата наук: Разработка алгоритмов семантической сегментации и классификации биомедицинских сигналов низкой размерности на основе машинного обучения
