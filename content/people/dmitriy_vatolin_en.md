Active: yes
Category: people
PersonType: researcher
Id: dmitriy_vatolin
Lang: en
Title: Dmitriy S. Vatolin
PersonOrder: 5
Position: Senior researcher
Email: dmitriy@graphics.cs.msu.ru
Photo: images/people_images/dmitriy_vatolin.jpg
ResearchInterests: Video and data compression, Video processing, 3D video, 
Projects: video_matting_benchmark
SelectedPublications: 

Dmitriy Vatolin graduated from the Applied Mathematics department of Moscow State University in 1996. He defended his Ph.D. thesis on computer graphics in 2000. A co-author of a book on data compression (Russian), published in 2003. A co-founder of compression.ru website — one of the biggest site on data compression and video processing in the world. From 2000 until 2006 took part in 5 start-up computer companies, in 3 as a co-founder, 4 of the companies are operating until now.


