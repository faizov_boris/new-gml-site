Active: yes
Category: projects
ProjectType: ongoing
Id: medical_image_analysis
ProjectOrder: 85
SmallPhoto: images/projects_images/medical_image_analysis_small.png
Photo: 
Title: Medical image analysis
ContactPerson: olga_senyukova
Team: olga_senyukova, anton_konushin, denis_zobnin
Lang: en
SelectedPublications: zubov2015segmentatsiya11238486, denis2015fast11238745, zobnin2015algoritm8602879, senyukova2014segmentation6529757, akhadov2012algoritm560947, akhadov2011diffuse556787, zobnin2016segmentation27904560

This research direction is devoted to development of the algorithms for automatic analysis of images of internal structures of human body. These problems are very challenging in computer vision due to few amounts of available labeled data, high inter-class variability, absence of color information and other issues. We try to incorporate the best findings in computer vision and machine learning and, at the same time, to develop new approaches that try to minimize the amount of the training data or simplify the procedure of its labeling. Our projects focus on magnetic resonance images (MRI) of human brain and computed tomography (CT) images of human heart.

## Topics
- Segmentation of brain lesions on MRI

![Image example]({static}/images/projects_images/medical_image_analysis_segmentation_brain_lesions.png)

- Registration of brain MRI

![Image example]({static}/images/projects_images/medical_image_analysis_registration_brain.png)

- Segmentation of brain and other images into anatomical structures

![Image example]({static}/images/projects_images/medical_image_analysis_segmentation_brain.png)
