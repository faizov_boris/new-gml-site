Active: yes
Category: projects
ProjectType: ongoing
Id: human_gait_recognition_in_video
ProjectOrder: 90
SmallPhoto: images/projects_images/human_gait_recognition_in_video_small.jpg
Photo: images/projects_images/human_gait_recognition_in_video_big.jpg
Title: Human gait recognition in video
ContactPerson: anton_konushin
Team: anton_konushin, anna_sokolova, ilya_petrov, vladimir_guzov
Lang: en
SelectedPublications: publ1


Human identification in video surveillance data is an important problem for security and counteraction to terrorism. However, the most common and widespread methods of biometrical identification, such as face recognition have limited application and are not usable in many real situations. For example, if several people wearing the same clothes (uniform) and having their faces hidden are observed, the only way to distinguish them from each other is by their gait, gestures, or build.

The gait recognition methods have been investigated since mid-nineties. The most promising approach achieving the best recognition quality is the 3D model approach, estimating static and dynamic features of human limbs motion in three-dimensional coordinates. The evaluation of these features requires human pose estimation, i.e. the position of all the main joints at each moment of time. Currently, it can be measured only with 3D sensors or multi-view systems working in controlled conditions. The gait recognition within the framework of video surveillance is impossible because of the low accuracy of human pose estimation in video.

The main goal of the project is the development of pose evaluation methods based on generative approach to improve the quality of pose estimation and the accuracy of gait and gesture recognition. The generative approach also allows to estimate the parameters of humans figure that can be used for build recognition. The second goal of the project is the investigation of neural network methods of gait recognition, that have already been successful in face recognition problem, but have not been applied to gait yet.

## Acknowledgements

This work was supported by grant RFBR \#16-29-09612 "Research and development of person identification methods based on gait, gestures and body build in video surveillance data".
