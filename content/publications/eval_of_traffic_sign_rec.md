Active: yes
Category: publications
id: eval_of_traffic_sign_rec
Date: 2021-01-01
Year: 2021
Image: images/traffic_sign_recognition_small.jpg
Title: Evaluation of Traffic Sign Recognition Methods Trained on Synthetically Generated Data
LabGroupIds: cv_group
PaperAuthors: Person. A, anton_konushin, Person. B
Journal: Advanced Concepts for Intelligent Vision Systems (Springer LNCS, Vol. 8192), 2011
DownloadLink: https://link.springer.com/chapter/10.1007/978-3-319-02895-8_52
BibTexId: moiseev2013evaluation5112310

Most of today’s machine learning techniques requires large manually labeled data. This problem can be solved by using synthetic images. Our main contribution is to evaluate methods of traffic sign recognition trained on synthetically generated data and show that results are comparable with results of classifiers trained on real dataset. To get a representative synthetic dataset we model different sign image variations such as intra-class variability, imprecise localization, blur, lighting, and viewpoint changes. We also present a new method for traffic sign segmentation, based on a nearest neighbor search in the large set of synthetically generated samples, which improves current traffic sign recognition algorithms.
