Active: yes
Category: courses
Id: computer_graphics
Lang: en
Title: Computer graphics
Teachers: vladimir_frolov, anton_konushin
Image: 
CourseOrder: 10

Course program include human optical system, color and light modeling, basic image processing and analisis, basics of signal processing, rendering pipeline, OpenGL, introduction to shaders, global illumination, ray tracing and radiosity methods.
