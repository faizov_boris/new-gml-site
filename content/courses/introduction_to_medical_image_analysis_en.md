Active: yes
Category: courses
Id: introduction_to_medical_image_analysis
Lang: en
Title: Introduction to medical image analysis
Teachers: olga_senyukova
Image: 
CourseOrder: 15

This course is devoted to medical problems, involving image analysis, and state-of-the-art algorithms used for solving these problems. Besides basic algorithms for segmentation, feature extraction and description, algorithms based on machine learning are presented. Also, image matching, or registration algorithms, that play an important role in medical image analysis, are presented. The course is accompanied by the practical task where the students are free in choosing the algorithms.
