Active: yes
Category: courses
Id: computer_vision_introduction
Lang: en
Title: Introduction to computer vision
Teachers: anton_konushin, vlad_shakhuro
Image: 
CourseOrder: 0

In this intoductory course basic methods and algorithms of computer vision are presented. Course program includes image formation process, image filtering, image segmentation, local features detection and matching, robust fitting methods, introduction to machine learning, image categorization, object detection, image retrieval, large image collections, optical flow estimation, background subtraction, object tracking, action recognition, computer vision for augmented reality, pose estimation in Microsoft Kinect. Course is accompanied by several practical tasks.
