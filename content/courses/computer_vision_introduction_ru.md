Active: yes
Category: courses
Id: computer_vision_introduction
Lang: ru
Title: Введение в компьютерное зрение
Teachers: anton_konushin, vlad_shakhuro
Image: 
CourseOrder: 0

В этом вводном курсе рассматриваются ключевые методы и алгоритмы обработки и распознавания изображений. В программу курса входят устройство зрительной системы человека и цифровой камеры, основы обработки изображений, введение в машинное обучение, классификация изображений и выделение объектов, таких как лица людей, пешеходы и автомобили, построение и использование больших коллекций изображений, поиск изображений по содержанию в интернете, использование многослойных нейронных сетей ("deep learning"), распознавание человека по лицу. Курс сопровождается домашними заданиями, выполняемыми на языке Python.
